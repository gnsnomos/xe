import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, tap, switchMap, finalize, filter, map } from 'rxjs/operators';
import { SearchService } from '../services/search.service';
import { TranslateService } from '@ngx-translate/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    public searchResults: any[] = [];
    public isLoading = false;
    public googleSearchActive = false;
    public errorMsg: string;

    private _lang = 'en';
    private _resultsLimit = 20;

    @ViewChild('searchInput', { static: true }) private _searchInput: ElementRef;

    constructor(
        private _searchService: SearchService,
        private _translate: TranslateService,
        private _deviceService: DeviceDetectorService,
        private _titleService: Title
    ) {
        // Set default language
        this._translate.setDefaultLang('en');

        // Get browser's language (if any)
        if (this._translate.getBrowserLang() !== undefined) {
            this._lang = this._translate.getBrowserLang();
        }

        // Set the translation language
        this._translate.use(this._lang);

        // Update window's title
        this._translate.get('app-title').subscribe((res: string) => {
            this._titleService.setTitle(res);
        });

        // Decrease results when mobile device is used
        if (this._deviceService.isMobile()) {
            this._resultsLimit = 10;
        }
    }

    public ngOnInit(): void {
        this._initSearch();
    }

    /**
     * Open Google search for the selected keyword
     */
    public onButtonClick(): void {
        const url = 'https://www.google.gr/search?q=' + this._searchInput.nativeElement.value;
        window.open(url, '_blank');
    }

    /**
     * Activate google search button and clear results on choice selection
     * @param $event 
     */
    public onLocationSelect($event: Event): void {
        this.googleSearchActive = true;
        this.searchResults = [];
    }

    private _initSearch(): void {
        fromEvent(this._searchInput.nativeElement, 'keyup')
            .pipe(
                tap(() => {                     // Reset values
                    this.googleSearchActive = false;
                }),
                map((event: any) => {
                    return event.target.value;  // Map search input value
                }),
                filter(res => res.length > 1),  // Continue if keyword's length is greater than 1
                debounceTime(500),              // 500ms delay
                tap(() => {                     // Reset values
                    this.errorMsg = '';
                    this.searchResults = [];
                    this.isLoading = true;
                }),
                // Send values to service
                switchMap(value => this._searchService.searchLocation(value, this._lang, this._resultsLimit)
                    .pipe(
                        finalize(() => {
                            this.isLoading = false;
                        }),
                    )
                )
            )
            .subscribe(data => {
                // Show results, otherwise display the error message
                this.errorMsg = '';
                if (data['entries'].length === 0) {
                    this.errorMsg = 'no-results';
                    this.searchResults = [];
                } else {
                    this.searchResults = data['entries'];
                }
            });
    }
}
