import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    private readonly _searchUrl = 'http://35.180.182.8/search';

    constructor(private _http: HttpClient) { }

    // Perform search
    public searchLocation(searchValue: string, language: string, resultsAmount: number = 10): Observable<object> {
        return this._http.get(this._searchUrl + '?language=' + language + '&keywords=' + searchValue + '&limit=' + resultsAmount);
    }
}
